import unittest
import traceback
import time
from tools import HTMLTestRunner
import logging
from tests.entity.system_admin.log_in_system.login import *
from tests.entity.group_manager.actions.group.group_actions import *
from tests.entity.group_manager.logs.threat_log import *
from tests.entity.group_manager.logs.app_log import *
from tests.entity.group_manager.logs.device_log import *
from tests.entity.group_manager.logs.cdr_log import *


def runTest():
  times = 0
  timeout = time.time() + 60*1  # 24h = 60*60*24
  timestr = time.strftime('%Y_%m_%d_%H.%M.%S', time.localtime(time.time()))

  filename = "./logs/"+timestr+".html"
  with open(filename, 'wb') as f:
    runner = HTMLTestRunner.HTMLTestRunner(
    stream=f,
    title=u'Test Report: {0}'.format(times),
    description=u'24 test case, test by TG'
    )
    logging.info('Test Times: {0}'.format(times))
    times += 1
    runner.run(suite())

# must do before running:

# kill eran

def suite():
  suite = unittest.TestSuite()
  tests = [
    # ////System admin Tests////
    # Login System
    # LoginSystem('test_sa_login'),
    # LoginSystem('test_sa_logout'),
    #
    # # ////Group Admin Tests////
    # # Actions
    GroupActions('test_gm_view_extension_from_group'),
    GroupActions('test_gm_edit_extension_from_group'),
    GroupActions('test_gm_download_group_devices_log'),
    GroupActions('test_gm_download_group_calls_log'),
    GroupActions('test_gm_download_group_threat_log'),

    # ////Group Logs tests////
    # Threat log
    GroupThreatLog('test_gm_threat_log_open_log'),
    # GroupThreatLog('test_gm_threat_log_attack_time_range'),
    # GroupThreatLog('test_gm_threat_log_filter_severity'),
    # GroupThreatLog('test_gm_threat_log_filter_threat_type'),
    # GroupThreatLog('test_gm_threat_log_filter_extension'),
    # GroupThreatLog('test_gm_threat_log_filter_model'),
    # GroupThreatLog('test_gm_threat_log_filter_os'),
    #
    # # App log
    # GroupAppLog('test_gm_app_log_expand_log'),
    # GroupAppLog('test_gm_app_log_filter_attack_time_range'),
    # GroupAppLog('test_gm_app_log_filter_app_name'),
    # GroupAppLog('test_gm_app_log_filter_name_space'),
    # GroupAppLog('test_gm_app_log_filter_app_version'),
    #
    # # Device Log
    # GroupDeviceLog('test_gm_device_log_expand_log'),
    # GroupDeviceLog('test_gm_devices_filter_risk_posture'),
    # GroupDeviceLog('test_gm_devices_filter_device_id'),
    # GroupDeviceLog('test_gm_devices_filter_extension'),
    # GroupDeviceLog('test_gm_devices_filter_created_at'),
    # GroupDeviceLog('test_gm_devices_filter_app_name'),
    # GroupDeviceLog('test_gm_devices_filter_os'),
    # GroupDeviceLog('test_gm_devices_filter_os_upgradeable'),
    # GroupDeviceLog('test_gm_devices_filter_os_vulnerable'),
    # GroupDeviceLog('test_gm_devices_filter_model'),
    # GroupDeviceLog('test_gm_devices_filter_last_seen'),
    #
    # # CDR log
    # GroupCdrLog('test_gm_cdr_filter_date'),
    # GroupCdrLog('test_gm_cdr_filter_call_date'),
    # GroupCdrLog('test_gm_cdr_filter_call_id'),
    # GroupCdrLog('test_gm_cdr_filter_source_number'),
    # GroupCdrLog('test_gm_cdr_filter_destination_number'),
    # GroupCdrLog('test_gm_cdr_filter_duration'),
    # GroupCdrLog('test_gm_cdr_filter_disposition'),
    # GroupCdrLog('test_gm_cdr_report_download')
  ]

  suite.addTests(tests)
  return suite

if __name__ == "__main__":
  try:
    runTest()
  except Exception as e:
    logging.info('Exception: {0}'.format(e))
    logging.debug('Exception: {0}'.format(traceback.format_exc()))
  finally:
    print('ManageiT Test Reports.')