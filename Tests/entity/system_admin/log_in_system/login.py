import time
import unittest
from selenium import webdriver
from tools.helper import *
from tools.browser_helper import *
from tools.credentials import *
from tools.routes import *
from browsermobproxy import Server


class LoginSystem(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy()
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.proxy = cls.server.create_proxy(params={"trustAllServers": "true"})
        cls.browser.implicitly_wait(10)

    def test_sa_login(self):
        self.browser.get("https://manageit-test2.assacnetworks.com")
        sa_log_in(self.browser)
        time.sleep(3)
        self.assertTrue(assert_by_url_method_status(sa_login_url, self.proxy, "POST", 200))

    def test_sa_logout(self):
        self.browser.get("https://manageit-test2.assacnetworks.com")
        time.sleep(5)
        sa_log_out(self.browser)
        time.sleep(3)
        self.assertTrue(assert_by_url_method_status(sa_log_out_url, self.proxy, "PUT", 200))
        self.browser.quit()


if __name__ == '__main__':
    unittest.main()