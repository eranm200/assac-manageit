import time
import unittest
from selenium import webdriver
from tools.helper import *
from tools.browser_helper import *
from browsermobproxy import Server


class ExtensionActions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy()
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.browser.implicitly_wait(10)


    def test_create_new_extension(self):
        self.browser.get("https://manageit-test2.assacnetworks.com")
        sa_log_in(self.browser)
        # create_new_extension(self.browser, '2323', 'erantest', '*Eran12345')
        time.sleep(20)
        self.assertTrue(assert_by_url_json("/api/user", self.proxy, "GET", 200, "1004"))

    def test_delete_extension(self):
        delete_new_extension(self.browser, 2323)
        time.sleep(20)
        # self.assertTrue(assert_by_url("/extensions", self.proxy, "POST", 201))
        self.proxy.close()
        self.server.stop()
        self.browser.quit()


if __name__ == '__main__':
    unittest.main()

