import unittest
from tools.helper import *
from tools.routes import *
from tools.browser_helper import *
from browsermobproxy import Server

class GroupThreatLog(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy()
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.browser.implicitly_wait(10)

    def test_gm_threat_log_open_log(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_threat_log_open_log(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_attack_time_range(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_threat_log_attack_time_range(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_severity(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_threat_log_filter_severity(self.browser)
        time.sleep(2)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_threat_type(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_threat_log_filter_threat_type(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_extension(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_threat_log_filter_extension(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_model(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_threat_log_filter_model(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_os(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_threat_log_filter_os(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_app_name(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_app_log_filter_app_name(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

    def test_gm_threat_log_filter_app_version(self):
        self.browser.get(manageit_url)
        time.sleep(2)
        gm_threat_log_filter_app_version(self.browser)
        time.sleep(4)
        self.assertTrue(assert_threat_log(gm_threat_log_url, self.proxy, "PUT", 200))

if __name__ == '__main__':
    unittest.main()