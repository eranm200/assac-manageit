import unittest
from tools.helper import *
from tools.routes import *
from tools.browser_helper import *
from browsermobproxy import Server

class GroupDeviceLog(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy()
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.browser.implicitly_wait(10)

    def test_gm_device_log_expand_log(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_device_log_open_log(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_risk_posture(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_risk_posture(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_device_id(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_device_id(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_extension(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_extension(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_created_at(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_created_at(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_app_name(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_app_name(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_os(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_os(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_os_upgradeable(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_os_upgradeable(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_os_vulnerable(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_os_vulnerable(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_model(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_model(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))

    def test_gm_devices_filter_last_seen(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_devices_filter_model(self.browser)
        time.sleep(2)
        self.assertTrue(assert_device_log(gm_device_log_url, self.proxy, "PUT", 200))


if __name__ == '__main__':
    unittest.main()