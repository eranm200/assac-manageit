import unittest
from tools.helper import *
from tools.routes import *
from tools.browser_helper import *
from browsermobproxy import Server

class GroupAppLog(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy()
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.browser.implicitly_wait(10)

    def test_gm_app_log_expand_log(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_app_log_open_log(self.browser)
        time.sleep(4)
        self.assertTrue(assert_app_log(gm_app_log_url, self.proxy, "POST", 200))

    def test_gm_app_log_filter_attack_time_range(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_app_log_attack_time_range(self.browser)
        time.sleep(4)
        self.assertTrue(assert_app_log(gm_app_log_url, self.proxy, "POST", 200))

    def test_gm_app_log_filter_app_name(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_app_log_filter_app_name(self.browser)
        time.sleep(4)
        self.assertTrue(assert_app_log(gm_app_log_url, self.proxy, "POST", 200))

    def test_gm_app_log_filter_name_space(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_app_log_filter_app_name(self.browser)
        time.sleep(4)
        self.assertTrue(assert_app_log(gm_app_log_url, self.proxy, "POST", 200))

    def test_gm_app_log_filter_app_version(self):
        self.browser.get(manageit_url)
        time.sleep(4)
        gm_app_log_filter_app_version(self.browser)
        time.sleep(4)
        self.assertTrue(assert_app_log(gm_app_log_url, self.proxy, "POST", 200))

if __name__ == '__main__':
    unittest.main()