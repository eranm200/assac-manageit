import unittest
from tools.helper import *
from tools.android_helper import *
from tools.browser_helper import *
from tools.routes import *
from browsermobproxy import Server


class GroupCdrLog(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        # Chrome driver options
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy()
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.browser.implicitly_wait(10)
        # Appium options
        # appium_driver.implicitly_wait(10)

    # needs logs to open
    def test_gm_cdr_log_expand_log(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_log_open_log(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_date(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_date(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_call_date(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_call_date(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_call_id(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_call_id(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_source_number(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_source_number(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_destination_number(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_destination_number(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_duration(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_duration(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_filter_disposition(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_filter_disposition(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))

    def test_gm_cdr_report_download(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_cdr_report_download(self.browser)
        time.sleep(4)
        self.assertTrue(assert_cdr_log(gm_cdr_log_url, self.proxy, "PUT", 200))


if __name__ == '__main__':
    unittest.main()