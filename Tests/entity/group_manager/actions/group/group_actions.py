import unittest
from tools.helper import *
from tools.routes import *
from tools.browser_helper import *

class GroupActions(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.server = Server(browser_mob_server)
        cls.server.start()
        cls.proxy = cls.server.create_proxy(params={"trustAllServers": "true"})
        cls.browser = webdriver.Chrome(chromedriver, options=create_chrome_options(cls.proxy.proxy))
        cls.proxy.new_har("req", options={'captureHeaders': True, 'captureContent': True})
        cls.browser.implicitly_wait(10)

    def test_gm_view_extension_from_group(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_view_extension_from_group(self.browser)
        time.sleep(7)
        self.assertTrue(assert_view_extension_from_group(gm_view_extension_from_group_url, self.proxy, "GET", 200, "1004"))

    def test_gm_edit_extension_from_group(self):
        self.browser.get(manageit_url)
        gm_log_in(self.browser)
        time.sleep(4)
        gm_edit_extension_from_gorup(self.browser, 'eran test after edit', '*Eran12345!!')
        time.sleep(15)
        self.assertTrue(assert_by_url_method_status(gm_edit_extension_from_gorup_url, self.proxy,  "PUT", 200))

    def test_gm_download_group_threat_log(self):
        # gm_log_in(self.browser)
        self.browser.get(manageit_url)
        gm_download_group_threat_log(self.browser)
        time.sleep(3)
        self.assertTrue(assert_by_url_method_status(gm_download_group_threat_log_url, self.proxy,  "PUT", 200))
        time.sleep(5)

    def test_gm_download_group_calls_log(self):
        self.browser.get(manageit_url)
        # gm_log_in(self.browser)
        gm_download_group_calls_log(self.browser)
        time.sleep(3)
        self.assertTrue(assert_by_url_method_status(gm_download_group_calls_log_url, self.proxy, "PUT", 200))
        time.sleep(5)

    def test_gm_download_group_devices_log(self):
        self.browser.get(manageit_url)
        gm_download_group_devices_log(self.browser)
        time.sleep(3)
        self.assertTrue(assert_by_url_method_status(gm_download_group_devices_log_url, self.proxy, "PUT", 200))
        time.sleep(5)

if __name__ == '__main__':
    unittest.main()