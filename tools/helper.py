import time
from tools.logger import Logger
from tools.credentials import *
from browsermobproxy import Server
from selenium import *


# //////// ManageiT simple actions /////////
def go_to_dashboard(driver):
    driver.find_element_by_xpath('//*[@id="root"]/section/aside/div[1]/div[2]/div[1]/ul/li[1]').click()

def open_side_menu(driver):
    driver.find_elements_by_class_name('ant-layout-sider-trigger').click()

def go_to_users_menu(driver):
    driver.find_element_by_class_name('ant-menu-submenu-title').click()
    time.sleep(2)
    driver.find_element_by_xpath('//*[@id="manage$Menu"]/li[1]').click()
    driver.find_element_by_class_name('ant-menu-submenu-title').click()

def open_shield_menu(driver):
    driver.find_element_by_xpath("//div[@class='ant-menu-submenu-title']//span[text()='Shield']").click()
    time.sleep(2)

def close_shield_menu(driver):
    driver.find_element_by_xpath("//div[@class='ant-menu-submenu-title']//span[text()='Shield']").click()
    time.sleep(2)

def go_to_cdr_log(driver):
    driver.find_element_by_xpath("/html/body/div[1]/section/aside/div[1]/div[2]/div[1]/ul/li[4]/div").click()
    time.sleep(2)
    driver.find_element_by_id("reports$Menu").click()

def cdr_log_date(driver):
    driver.find_element_by_id('calldate').click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-prev-year-btn').click()
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-left']//td[4][@class='ant-calendar-cell']").click()
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-right']//tr[1]//td[@class='ant-calendar-cell ant-calendar-in-range-cell']").click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-ok-btn').click()
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div/div[2]/div[1]/div/form/div[3]/button').click()

def go_to_device_log(driver):
    open_shield_menu(driver)
    time.sleep(3)
    driver.find_element_by_xpath("//ul[@class='ant-menu ant-menu-sub ant-menu-inline']//li[3][@class='ant-menu-item']").click()
    time.sleep(2)
    close_shield_menu(driver)

def device_log_date(driver):
    driver.find_element_by_xpath('/html/body/div/section/section/main/div[2]/div/div[1]/div[2]/div[1]/div/form/div/div/div[2]/div/span/span/span/input[1]').click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-prev-year-btn').click()
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-left']//td[2][@class='ant-calendar-cell']").click()
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-right']//tr[1]//td[@class='ant-calendar-cell ant-calendar-in-range-cell']").click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-ok-btn').click()
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div[1]/div[2]/div[1]/div/form/button').click()

def go_to_app_log(driver):
    open_shield_menu(driver)
    driver.find_element_by_xpath("//ul[@class='ant-menu ant-menu-sub ant-menu-inline']//li[2][@class='ant-menu-item']").click()
    close_shield_menu(driver)

def app_log_date(driver):
    driver.find_element_by_xpath('/html/body/div/section/section/main/div[2]/div/div[1]/div[2]/div[1]/div/form/div/div/div[2]/div/span/span/span/input[1]').click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-prev-year-btn').click()
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-left']//td[4][@class='ant-calendar-cell']").click()
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-right']//tr[1]//td[@class='ant-calendar-cell ant-calendar-in-range-cell']").click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-ok-btn').click()
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div[1]/div[2]/div[1]/div/form/button').click()

def go_to_threat_log(driver):
    open_shield_menu(driver)
    time.sleep(3)
    driver.find_element_by_xpath("//ul[@class='ant-menu ant-menu-sub ant-menu-inline']//li[1][@class='ant-menu-item']").click()
    time.sleep(1)
    close_shield_menu(driver)

def threat_log_date(driver):
    driver.find_element_by_xpath('/html/body/div/section/section/main/div[2]/div/div[1]/div[2]/div[1]/div/form/div/div/div[2]/div/span/span/span/input[1]').click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-prev-year-btn').click()
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-left']//td[2][@class='ant-calendar-cell']").click()
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-calendar-range-part ant-calendar-range-right']//tr[1]//td[@class='ant-calendar-cell ant-calendar-in-range-cell']").click()
    time.sleep(2)
    driver.find_element_by_class_name('ant-calendar-ok-btn').click()
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div[1]/div[2]/div[1]/div/form/button').click()


# ////////assert by type////////
# get response
def get_response_by_url(url, proxy, method):
    for log in proxy.har["log"]["entries"]:
        if url in log["request"]["url"] and log["request"]["method"] == method:
            if log["response"]["status"] == 0:
                Logger.log("request {} did not finish. ".format(url))
            return log["response"]

# assertion types
def assert_by_url_method_status(url, proxy, method, status):
    response = get_response_by_url(url, proxy, method)
    return response["status"] == status

def assert_view_extension_from_group(url, proxy, method, status, extension_num):
    response = get_response_by_url(url, proxy, method)
    return response["status"] == status and '"extension":' + extension_num in response["content"]["text"]

def assert_threat_log(url, proxy, method, status):
    response = get_response_by_url(url, proxy, method)
    return response["status"] == status

def assert_app_log(url, proxy, method, status):
    response = get_response_by_url(url, proxy, method)
    return response["status"] == status

def assert_device_log(url, proxy, method, status):
    response = get_response_by_url(url, proxy, method)
    return response["status"] == status

def assert_cdr_log(url, proxy, method, status):
    response = get_response_by_url(url, proxy, method)
    return response["status"] == status

# //////// System Admin functions /////////
# login system
def sa_log_in(driver):
    time.sleep(3)
    driver.find_element_by_id('extension').send_keys(system_admin_extension)
    driver.find_element_by_id('password').send_keys(system_admin_password)
    # ////privacy policy button////
    driver.find_element_by_xpath('//*[@id="root"]/section/main/div/div[1]/div/section/div[2]/div/div/div/div/form/div[3]/label/span[1]/input').click()
    driver.find_element_by_xpath('//*[@id="root"]/section/main/div/div[1]/div/section/div[2]/div/div/div/div/form/div[4]/button').click()

def sa_log_out(driver):
    driver.find_element_by_xpath('//*[@id="root"]/section/section/header/div/div[2]').click()
    time.sleep(3)
    driver.find_element_by_xpath("//ul[@class='ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical']//li[7][@class='ant-dropdown-menu-item']").click()

# //////// System Admin Actions /////////
# Extension
def sa_create_new_extension(driver, user_extension,  user_name, password):
    driver.find_element_by_class_name('ant-menu-submenu-title').click()
    driver.find_element_by_xpath('//*[@id="manage$Menu"]/li[1]').click()
    driver.find_element_by_class_name('ant-btn').click()
    # time.sleep(5)
    driver.find_element_by_id('extension').send_keys(user_extension)
    driver.find_element_by_id('name').send_keys(user_name)
    driver.find_element_by_id('secret').clear()
    driver.find_element_by_id('secret').send_keys(password)
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div/div/div/div[2]/form/fieldset/div/div/div[1]/div[4]/div/div[2]/div/span/div/div/div/div').click()
    time.sleep(2)
    driver.find_element_by_xpath('/html/body/div[3]/div/div/div/ul/li[2]').click()
    time.sleep(2)
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div/div/div/div[2]/form/fieldset/div/div/div[5]/div/button[1]').click()

def sa_delete_new_extension(driver, extension_num):
    driver.find_element_by_class_name('ant-menu-submenu-title').click()
    driver.find_element_by_xpath('//*[@id="manage$Menu"]/li[1]').click()
    time.sleep(7)
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div[2]/div[2]/div[1]/div[2]/span/input').send_keys(extension_num)
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div/div/div/div/div/table/tbody/tr/td[8]/span/button[2]').click()
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/div[3]/div/div/div/div[2]/div/div/div[2]/button[2]').click()

# Group
# //////// System Admin Log /////////
# Threat Log
# App log
# Devices


# //////// Group Manager Actions /////////
# Group Manager Actions
# login system
def gm_log_in(driver):
    time.sleep(3)
    driver.find_element_by_id('extension').send_keys(group_manager_extension)
    driver.find_element_by_id('password').send_keys(group_manager_password)
    # ////privacy policy button////
    driver.find_element_by_xpath('//*[@id="root"]/section/main/div/div[1]/div/section/div[2]/div/div/div/div/form/div[3]/label/span[1]/input').click()
    driver.find_element_by_xpath('//*[@id="root"]/section/main/div/div[1]/div/section/div[2]/div/div/div/div/form/div[4]/button').click()

def gm_log_out(driver):
    driver.find_element_by_xpath('//*[@id="root"]/section/section/header/div/div[2]').click()
    time.sleep(3)
    driver.find_element_by_xpath("//ul[@class='ant-dropdown-menu ant-dropdown-menu-light ant-dropdown-menu-root ant-dropdown-menu-vertical']//li[7][@class='ant-dropdown-menu-item']").click()


# Extension
def gm_view_extension_from_group(driver):
    go_to_users_menu(driver)
    time.sleep(6)
    driver.find_element_by_xpath('//table/tbody/tr/td[8]').click()

def gm_edit_extension_from_gorup(driver, name, secret):
    go_to_users_menu(driver)
    time.sleep(3)
    driver.find_element_by_xpath('//table/tbody/tr/td[8]').click()
    time.sleep(2)
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div/div/div/div[1]/div/button').click()
    driver.find_element_by_id('name').clear()
    driver.find_element_by_id('name').send_keys(name)
    driver.find_element_by_id('secret').clear()
    driver.find_element_by_id('secret').send_keys(secret)
    driver.find_element_by_xpath('/html/body/div[1]/section/section/main/div[2]/div/div/div/div/div/div[2]/form/fieldset/div/div/div[5]/div/button[1]').click()

def gm_download_group_devices_log(driver):
    go_to_users_menu(driver)
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/div/section/section/main/div[2]/div/div/div[2]/div/div/div/div/div[2]/div/button[2]').click()

def gm_download_group_threat_log(driver):
    go_to_users_menu(driver)
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/div/section/section/main/div[2]/div/div/div[2]/div/div/div/div/div[2]/div/button[1]').click()

def gm_download_group_calls_log(driver):
    go_to_users_menu(driver)
    time.sleep(3)
    driver.find_element_by_xpath('/html/body/div/section/section/main/div[2]/div/div/div[2]/div/div/div/div/div[2]/div/button[3]').click()

# //////// Group Manager Logs /////////
# Threat Log
def gm_threat_log_open_log(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    driver.find_element_by_xpath("//div[@class='ant-spin-nested-loading']//tr[@class='ant-table-row ant-table-row-level-0']").click()

def gm_threat_log_attack_time_range(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)

def gm_threat_log_filter_severity(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Severity']").click()

def gm_threat_log_filter_threat_type(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Threat Type']").click()

def gm_threat_log_filter_extension(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(3)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Extension']").click()

def gm_threat_log_filter_model(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Model']").click()

def gm_threat_log_filter_os(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='OS']").click()

def gm_threat_log_filter_app_name(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='App Name']").click()

def gm_threat_log_filter_app_version(driver):
    go_to_threat_log(driver)
    threat_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='App Version']").click()

# App Log
def gm_app_log_open_log(driver):
    go_to_app_log(driver)
    app_log_date(driver)
    driver.find_element_by_xpath("//div[@class='ant-table-wrapper utils__scrollTable']//tr[@class='ant-table-row ant-table-row-level-0']").click()

def gm_app_log_attack_time_range(driver):
    go_to_app_log(driver)
    app_log_date(driver)

def gm_app_log_filter_app_name(driver):
    go_to_app_log(driver)
    app_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='App Name']").click()

def gm_app_log_filter_app_version(driver):
    go_to_app_log(driver)
    app_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='App Version']").click()

def test_gm_app_log_filter_name_space(driver):
    go_to_app_log(driver)
    app_log_date(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Name Space']").click()

# Device Log
def gm_device_log_open_log(driver):
    go_to_device_log(driver)

def gm_devices_filter_risk_posture(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Risk Posture']").click()

def gm_devices_filter_device_id(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Device id']").click()

def gm_devices_filter_extension(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Extension']").click()

def gm_devices_filter_created_at(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Created At']").click()

def gm_devices_filter_app_name(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='App Name']").click()

def gm_devices_filter_os(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='OS']").click()

def gm_devices_filter_os_upgradeable(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='OS Upgradeable']").click()

def gm_devices_filter_os_vulnerable(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='OS Vulnerable']").click()

def gm_devices_filter_model(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Model']").click()

def gm_devices_filter_last_seen(driver):
    go_to_device_log(driver)
    time.sleep(2)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Last Seen']").click()

# CDR Report log
def gm_cdr_log_open_log(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)

def gm_cdr_filter_date(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Call Date']").click()

def gm_cdr_filter_source_number(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Source Number']").click()

def gm_cdr_filter_destination_number(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Destination Number']").click()

def gm_cdr_filter_duration(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Duration']").click()

def gm_cdr_filter_disposition(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Disposition']").click()

def gm_cdr_report_download(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='utils__title']//button[@class='ant-btn ant-btn-secondary']").click()

def gm_cdr_filter_call_id(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Caller ID']").click()

def gm_cdr_filter_call_date(driver):
    go_to_cdr_log(driver)
    cdr_log_date(driver)
    time.sleep(4)
    driver.find_element_by_xpath("//div[@class='ant-table-column-sorters']//span[text()='Call Date']").click()






















