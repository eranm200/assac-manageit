import time
from appium import webdriver

# Setup
desired_caps_xiaomi = {
    'platformName': 'Android',
    'platformVersion': '8.1',
    'automationName': 'UiAutomator1',
    'deviceName': 'Xiaomi Redmi Note 6 Pro API 27',
    'appPackage': 'com.assacnetworks.shieldit',
    'appActivity': 'com.assacnetworks.shieldit.presentation.splash.SplashActivity',
    'autoGrantPermissions': True,
    'deviceid': '192.168.1.170',
    # 'deviceid': '9e73e2d',
    'noReset': True,
    'fullReset': False,
    'newCommandTimeout': '4000',
    'adbExecTimeout': '80000',
    'systemPort': '5000'
}

# android settings
desired_caps = desired_caps_xiaomi
main_port = 'http://127.0.0.1:4723/wd/hub'

# appium driver
# appium_driver = webdriver.Remote(main_port, desired_caps)


# Android Actions
# Call System
def android_call_from_dialer_1002(driver):
    driver.find_element_by_id('com.assacnetworks.shieldit:id/one').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/zero').click()
    driver.find_element_by_id('com.assacnetworks.shieldit:id/two').click()
    time.sleep(4)
    driver.find_element_by_id('com.assacnetworks.shieldit:id/call_ic').click()
    time.sleep(2)


