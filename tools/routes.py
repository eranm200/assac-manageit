# ////////// this file will contain all the url routing //////////

# ////System admin Tests////
# Login System
sa_login_url = "/api/user/login"

sa_log_out_url = "/api/user/logout"

# ////Group Admin Tests////
# Actions
gm_edit_extension_from_gorup_url = "/api/extensions"

gm_view_extension_from_group_url = "/api/extensions"

gm_download_group_devices_log_url = "/api/shield/devices"

gm_download_group_calls_log_url = "/api/reports/cdr"

gm_download_group_threat_log_url = "/api/shield/threats/csv"

# Logs
# threat log
gm_threat_log_url = "/api/shield/threats/filters"
# App Log
gm_app_log_url = "/api/shield/malwares"
# Device Log
gm_device_log_url = "/api/shield/devices"
# Cdr Log
gm_cdr_log_url = "/api/reports/cdr"

