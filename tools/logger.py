

class Logger:

    LABEL_INFO = 'INFO'

    @classmethod
    def log(cls, msg, label=LABEL_INFO):
        print("[{}] {}".format(label, msg))
