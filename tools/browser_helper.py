from selenium import webdriver
from tools.helper import *


# ////////chrome driver/////////
def create_chrome_options(proxy):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--proxy-server={}".format(proxy))
    chrome_options.add_argument("ignore-certificate-errors")
    # chrome_options.add_argument("user-data-dir=C:/Users/user_name/AppDataLocal/Google/Chrome/User Data/Default")///save cache
    return chrome_options

# ////////browser mob server path////////
manageit_url = "https://manageit-test2.assacnetworks.com/#/dashboard"
browser_mob_server = '/Users/eranmizrahi/Desktop/browsermob-proxy-2.1.4/bin/browsermob-proxy'
chromedriver = '/Users/eranmizrahi/chromedriver'






